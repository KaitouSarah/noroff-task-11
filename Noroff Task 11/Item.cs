﻿using System;

using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_11
{
    class Item
    {
        private string itemName;
        private double price;
        private string description;

        public Item (string item, double price, string description)
        {
            this.itemName = item;
            this.Price = price;
            this.Description = description;
        }

        public string ItemName { get => itemName; set => itemName = value; }
        public double Price { get => price; set => price = value; }
        public string Description { get => description; set => description = value; }
    }
}
