﻿using System;
using System.Collections.Generic;

namespace Noroff_Task_11
{
    class Program
    {
        static void Main(string[] args)
        {
            //Setting up "database"
            Customer customer1 = new Customer("David Thorne");
            CreditCard creditCard1 = new CreditCard(1000, customer1.Name, "Bank Norwegian", "1111 2222 3333 4444", 420);
            DebetCard debetCard1 = new DebetCard(120, customer1.Name, "DNB Bank ASA", "4444 3333 2222 1111", 917);
            CashWallet cashWallet1 = new CashWallet();
            cashWallet1.addMoney(new FiftyDollars());
            cashWallet1.addMoney(new QuarterDollar());
            customer1.CreditCard = creditCard1;
            customer1.DebetCard = debetCard1;
            customer1.CashWallet = cashWallet1;
            customer1.CashWallet.printWalletContent();

            List<Item> inventory = new List<Item>();
            inventory.Add(new Item("Samsung", 39, "Way to stand out!."));
            inventory.Add(new Item("Huawei", 34, "Thank you for contributing to China's mass survaliance."));
            inventory.Add(new Item("Apple", 139856, "You usless twat."));
            //End of setting up "database"

            //Counts items listed in console
            int counter = 1;

            //Prints all inventory for user
            foreach (Item item in inventory)
            {
                Console.WriteLine($"Press {counter} to buy an {item.ItemName} for {item.Price}$");
                counter++;
            }

            //Creating variables to avoid duplicate calls for them
            int userInputItem = Int32.Parse(Console.ReadLine());
            Item selectedItem = inventory[userInputItem - 1];
            double itemPrice = selectedItem.Price;
            string itemName = selectedItem.ItemName;
            string itemDescription = selectedItem.Description;

            Console.WriteLine("How would you like to pay for that? Press 1 for credit card, 2 for debet card or 3 for cash: ");
            int userInputPayment = Int32.Parse(Console.ReadLine());


            //World's longest switch for handling user input
            switch(userInputPayment)
            {
                //Pay with credit card
                case 1:
                    if (customer1.HasCreditCard())
                    {
                        if (customer1.CreditCard.CreditLimit >= itemPrice)
                        {
                            customer1.CreditCard.updateCreditLimit(itemPrice, true);
                            Console.WriteLine($"You bought a {itemName}. {itemDescription}");
                        } else
                        {
                            Console.WriteLine("Not enough $$$$.");
                        }
                    } else
                    {
                        Console.WriteLine("You actually need to have a debet card to pay with it.");
                    }
                    break;

                //Pay with debet card
                case 2:
                    if (customer1.HasDebetCard())
                    {
                        if (customer1.DebetCard.Balance >= itemPrice)
                        {
                            customer1.DebetCard.updateBalance(itemPrice, true);
                            Console.WriteLine($"You bought a {itemName}. {itemDescription}");
                        }
                        else
                        {
                            Console.WriteLine("Not enough $$$$.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("You actually need to have a credit card to pay with it.");
                    }
                    break;

                //Pay with cash
                case 3:
                    if (customer1.HasCashWallet())
                    {
                        if (customer1.CashWallet.balanceWallet() >= itemPrice)
                        {
                            customer1.CashWallet.updateWallet(itemPrice, true);
                            Console.WriteLine($"You bought an {itemName}. {itemDescription}");
                            customer1.CashWallet.printWalletContent();
                        }
                    } else
                    {
                        Console.WriteLine("You actually need a cash wallet to pay with it.");
                    }
                    break;

                //User chose invalid payment method
                default:
                    Console.WriteLine("You had 3 options, dimwit.");
                    break;
            }
            //end world's longest switch

            
        }

    }
}

