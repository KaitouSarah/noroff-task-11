﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_11
{
    class CreditCard : Card
    {
        //limit in USD
        private double creditLimit;

        public CreditCard(double creditLimit, string cardHoldersName, string bank, string cardNumber, int cvc) 
            : base(cardHoldersName, bank, cardNumber, cvc)
        {
            this.CreditLimit = creditLimit;
        }

        public void updateCreditLimit(double amount, Boolean purchase)
        {
            if (purchase)
            {
                creditLimit -= amount;
            } else
            {
                creditLimit += amount;
            }
            Console.WriteLine($"Your new credit card limit is {creditLimit}.");
        }

        public double CreditLimit { get => creditLimit; set => creditLimit = value; }
    }
}


