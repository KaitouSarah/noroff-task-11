﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_11
{
    class DebetCard : Card
    {
        double balance;

        public DebetCard (double balance, string cardHoldersName, string bank, string cardNumber, int cvc)
            : base(cardHoldersName, bank, cardNumber, cvc)
        {
            this.Balance = balance;
        }

        public void updateBalance(double amount, Boolean purchase)
        {
            if (purchase)
            {
                balance -= amount;
            }
            else
            {
                balance += amount;
            }
            Console.WriteLine($"Your new debet card balance is {balance}.");
        }

        public double Balance { get => balance; set => balance = value; }
    }
}
