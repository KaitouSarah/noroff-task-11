﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_11
{
    class Cash
    {
        private double value;
        private string currency;

        public Cash(double value, string currency)
        {
            this.Value = value;
            this.Currency = currency;
        }

        public double Value { get => value; set => this.value = value; }
        public string Currency { get => currency; set => currency = value; }
    }
}
