﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_11
{
    class CashWallet
    {
        //List of notes and coins in wallet
        private List<Cash> cashMoney;

        public CashWallet()
        {
            CashMoney = new List<Cash>();
        }
        //Adding a note or coin to wallet
        public void addMoney(Cash cash)
        {
            CashMoney.Add(cash);
        }

        //Checking balance of wallet
        public double balanceWallet()
        {
            double balance = 0.0;
            foreach (Cash cash in cashMoney)
            {
                balance += cash.Value;
            }

            return balance;
        }

        //Updating balance in wallet after using or getting money
        public void updateWallet(double amount, Boolean purchase)
        {
            List<Cash> newCashMoney = new List<Cash>();
            double balance = balanceWallet();

            if (purchase)
            {
                balance -= amount;
            } else
            {
                balance += amount;
            }

            //Will round up to closest .25$ when updating wallet for customer.
            while (balance >= 0.125)
            {
                if (balance >= 100)
                {
                    newCashMoney.Add(new HundredDollars());
                    balance -= 100;
                }
                else if (balance >= 50)
                {
                    newCashMoney.Add(new FiftyDollars());
                    balance -= 50;
                }
                else if (balance >= 20)
                {
                    newCashMoney.Add(new TwentyDollars());
                    balance -= 20;
                }
                else if (balance >= 10)
                {
                    newCashMoney.Add(new TenDollars());
                    balance -= 10;
                }
                else if (balance >= 5)
                {
                    newCashMoney.Add(new FiveDollars());
                    balance -= 5;
                }
                else if (balance >= 1)
                {
                    newCashMoney.Add(new OneDollar());
                    balance -= 1;
                }
                else
                {
                    newCashMoney.Add(new QuarterDollar());
                    balance -= 0.25;
                }
            }

            cashMoney = newCashMoney;
            Console.WriteLine($"Your new cash wallet balance is {balanceWallet()}.");
        }

        //Very simple method to print contents of the wallet
        public void printWalletContent()
        {
            int hundreds = 0;
            int fiftys = 0;
            int twentys = 0;
            int tens = 0;
            int fives = 0;
            int ones = 0;
            int quarters = 0;

            foreach (Cash cash in cashMoney)
            {
                if (cash.Value == 100)
                {
                    hundreds++;
                } else if (cash.Value == 50)
                {
                    fiftys++;
                } else if(cash.Value == 20)
                {
                    twentys++;
                } else if (cash.Value == 10)
                {
                    tens++;
                } else if(cash.Value == 5)
                {
                    fives++;
                } else if(cash.Value == 1)
                {
                    ones++;
                } else if (cash.Value == 0.25)
                {
                    quarters++;
                }
            }
            Console.WriteLine($"Your wallet contains {hundreds} 100 Dollar notes, {fiftys} 50 Dollar notes, " +
                $"{twentys} 20 Dollar notes, {tens} 10 Dollar notes, {fives} 5 Dollar notes, {ones} 1 Dollar notes," +
                $"and {quarters} Quarter Dollar coins.");
        }

        //Get&set
        internal List<Cash> CashMoney
        {
            get => cashMoney; set => cashMoney = value;
        }
    }
}
