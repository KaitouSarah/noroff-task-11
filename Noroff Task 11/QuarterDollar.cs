﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_11
{
    class QuarterDollar : Cash
    {
        public QuarterDollar() : base(0.25, "Quarter dollar")
        {

        }
    }
}
