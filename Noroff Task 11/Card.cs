﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_11
{
    class Card
    {
        string cardholderName;
        string bank;
        string cardNumber;
        int cvc;

        public Card (string cardholderName, string bank, string cardNumber, int cvc)
        {
            this.bank = bank;
            this.cardNumber = cardNumber;
            this.cvc = cvc;
        }

        public string Bank { get => bank; set => bank = value; }
        public string CardNumber { get => cardNumber; set => cardNumber = value; }
        public int Cvc { get => cvc; set => cvc = value; }
        public string CardholderName { get => cardholderName; set => cardholderName = value; }
    }
}
