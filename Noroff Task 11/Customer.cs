﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_11
{
    class Customer
    {
        string name;
        CreditCard creditCard;
        DebetCard debetCard;
        CashWallet cashWallet;

        public Customer(string name) 
        {
            this.Name = name;
            creditCard = null;
            debetCard = null;
            CashWallet = null;
        }

        public Boolean HasCreditCard()
        {
            return (creditCard != null);
        }

        public Boolean HasDebetCard()
        {
            return (debetCard != null);
        }

        public Boolean HasCashWallet()
        {
            return (cashWallet != null);
        }

        public string Name { get => name; set => name = value; }
        internal CreditCard CreditCard { get => creditCard; set => creditCard = value; }
        internal DebetCard DebetCard { get => debetCard; set => debetCard = value; }
        internal CashWallet CashWallet { get => cashWallet; set => cashWallet = value; }
    }
}
